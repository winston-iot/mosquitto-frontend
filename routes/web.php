<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

//Auth
Route::get('/home', 'HomeController@index')->name('home');
//Manage mosquitto Users
Route::get('/users', 'UserController@index')->name('Users');
Route::get('/users/create', 'UserController@create')->name('Users');
Route::get('/users/delete', 'UserController@delete')->name('Users');

