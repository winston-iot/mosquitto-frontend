<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        return view('users.users');
    }
    public function create(){
        return view('users.userscreate');
    }
    public function delete(){ 
        return view('users.usersdelete');
    }
}
